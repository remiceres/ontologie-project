**Objectif :** Mise en œuvre des techniques et technologies de l’Ingénierie des connaissances pour la réalisation d’un prototype d’application métier.

**Notions connexes :**
-   Modèle de données RDF,
-   langage de requêtage SPARQL,
-   API de programmation.

**Rapport de projet :** [rapport.pdf](./rapport/rapport.pdf)