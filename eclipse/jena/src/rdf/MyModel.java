package rdf;

import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerRegistry;
import org.apache.jena.util.FileManager;

public class MyModel
{
	private final String DIRECTORY = "./resources/models/";
	private final String QUERIES_DIRECTORY = "./resources/queries/";

	private final Reasoner REASONER = ReasonerRegistry.getOWLReasoner();

	private Model model;
	private Model inf_model;

	// Constructors
	MyModel(String onto_path, String instance_path)
	{
		// load model
		this.model = ModelFactory.createDefaultModel();

		InputStream in_onto = FileManager.get().open(DIRECTORY + onto_path);
		InputStream in_instance = FileManager.get().open(DIRECTORY + instance_path);

		model.read(new SequenceInputStream(in_onto, in_instance), null, "TTL");
		// inference model
		this.inf_model = ModelFactory.createInfModel(REASONER, this.model);
	}

	// Methods
	public void execute(String querie_path) throws IOException
	{

		// open and read queries file
		InputStream in = FileManager.get().open(QUERIES_DIRECTORY + querie_path);
		String querie_string;
		querie_string = IOUtils.toString(in, Charset.forName("UTF-8"));

		// Create a query object
		org.apache.jena.query.Query query = QueryFactory.create(querie_string);
		QueryExecution qexec = QueryExecutionFactory.create(query, inf_model);

		// Execute Query and print result
		org.apache.jena.query.ResultSet rs = qexec.execSelect();
		ResultSetFormatter.out(System.out, (org.apache.jena.query.ResultSet) rs, query);

		qexec.close();
	}

}
