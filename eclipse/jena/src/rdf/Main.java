package rdf;

import java.util.Scanner;

public class Main
{

	static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args)
	{
		// get user entry
		System.out.print("Enter the name of the model file: ");
		String onto = scanner.next();

		System.out.print("Enter the name of the instance file: ");
		String instance = scanner.next();

		// load model
		MyModel my_model = new MyModel(onto, instance);

		String request_file;

		do
		{
			// User entry

			System.out.print("\nEnter the name of the query file: ");
			request_file = scanner.next();

			try
			{
				my_model.execute(request_file);
			} catch (Exception e)
			{
				System.err.println("Impossible d'éxécuté la requête : " + request_file);
			}

		} while (!request_file.equals("q"));

		scanner.close();

	}
}
