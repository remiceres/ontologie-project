\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {french}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Pr\IeC {\'e}sentation du sujet}{2}{section.1.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Motivations et objectifs}{3}{section.1.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Mise en place de l'ontologie}{4}{chapter.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Mod\IeC {\'e}lisation de la T-box}{4}{section.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Classes}{4}{subsection.2.1.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}Relations}{5}{subsection.2.1.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{craft}{5}{section*.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{drop}{6}{section*.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Constitution de la A-box}{6}{section.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Mise en place des r\IeC {\`e}gles SWRL}{7}{section.2.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Illustration d\IeC {\textquoteright }utilisation}{9}{chapter.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Conclusion}{11}{chapter.4}% 
